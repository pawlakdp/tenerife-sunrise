#!/bin/bash
DAY=`date +"%Y%m%d"`
DT=`date +"%Y%m%d_%H%M%S"`
DSTDIR="/home/drago/tenerife_sunrise/$DAY"
DSTFILE="$DSTDIR/tensun_$DT.png"
if [ ! -d $DSTDIR ];
then
    mkdir $DSTDIR
fi
wget https://vega.slooh.com/images/widgets/webcams/teideCam.png -O $DSTFILE
cd $DSTDIR
convert -delay 3 -loop 0 *.png $DAY.gif
#
# EOF
#