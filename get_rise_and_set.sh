#!/bin/bash
#day=`date +"%e" | sed 's/\ //g'`
#month=`LANG=en_EN date +"%B" | awk '{print tolower($0)}'`
#year=`date +"%Y"`
#echo "month: $month"
#URL="http://www.sunrise-and-sunset.com/en/sun/spain/santa-cruz-de-tenerife/$year/$month/$day"

#
today=`date +"%Y-%m-%d"`
URL="https://api.sunrise-sunset.org/json?lat=28.273056&lng=-16.639444&date=2017-11-08&formatted=0&date=$today"
echo "URL=$URL"
wget $URL
