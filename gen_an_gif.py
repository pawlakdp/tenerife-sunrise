#!/usr/bin/env python
import imageio
import glob

images = []

fileList = glob.glob('*.png') #star grabs everything, can change to *.png for only png files
fileList.sort()

for filename in fileList:
    images.append(imageio.imread(filename))
imageio.mimsave('movie.gif', images)